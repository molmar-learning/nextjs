import Link from "next/link";

function EventItem({id, title, date, location, image, ...props}){

    const humanReadableDate = new Date(date).toLocaleDateString('hu-HU', {
        day: 'numeric',
        month: 'long',
        year: 'numeric'
    });

    const formattedAddress = location.replace(', ', '\n');

    return (
        <li key={id}>
            <img src={'/' + image} alt={title} />
            <div>
                <div>
                    <h2>{title}</h2>
                    <div>
                        <time> {humanReadableDate} </time>
                    </div>
                    <div>
                        <address> {formattedAddress} </address>
                    </div>
                </div>
                <div>
                    <Link
                        href={{
                            pathname: '/events/[id]',
                            query: { id: id },
                        }}
                    >
                        Explore Event
                    </Link>
                </div>
            </div>
        </li>
    )
}

export default EventItem;