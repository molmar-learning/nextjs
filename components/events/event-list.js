import EventItem from "./event-item";

function EventList({events, ...props}){
    return (
        <ul>
            {events.map(event => <EventItem
                id={event.id}
                title={event.title}
                location={event.location}
                date={event.date}
                image={event.image}
            />)}
        </ul>
    )
}

export default EventList;